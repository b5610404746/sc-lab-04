package lab04;


public class Control {

	Allstar all = new Allstar();

	public String build1(int num) {
		String a1 = all.star1(num);
		return a1;
	}

	public String build2(int num) {
		String a2 = all.star2(num);
		return a2;
	}

	public String build3(int num) {
		String a3 = all.star3(num);
		return a3;
	}

	public String build4(int num) {
		String a4 = all.star4(num);
		return a4;
	}

	public String build5(int num) {
		String a5 = all.star5(num);
		return a5;
	}

}
