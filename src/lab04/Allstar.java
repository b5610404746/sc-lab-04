package lab04;

public class Allstar {
	String star ="*";
	public String star1(int num){
		String star1="";
		for(int i=1; i<=num; i++){
			for(int j = 1; j<=num+1; j++){
				star1+=star;
			}
			star1+="\n";
		}
		return star1;
	}
	
	public String star2(int num){
		String star2="";
		for(int i=1; i<=num; i++){
			for(int j = 1; j<=num-1; j++){
				star2+=star;
			}
			star2+="\n";
		}
		return star2;
	}
	
	public String star3(int num){
		String star3="";
		for(int i=1; i<=num; i++){
			for(int j = 1; j<=i; j++){
				star3+=star;
			}
			star3+="\n";
		}
		return star3;
	}
	
	public String star4(int num){
		String star4="";
		for(int i=1; i<=num; i++){
			for(int j = 1; j<=num; j++){
				if(j%2 == 0){
					star4+=star;
				}
				else{
					star4+="-";
				}
			}
			star4+="\n";
		}
		return star4;
	}
	
	public String star5(int num){
		String star5="";
		for(int i=1; i<=num; i++){
			for(int j = 1; j<=num; j++){
				if((i+j)%2 == 0){
					star5+=star;
				}
				else{
					star5+=" ";
				}
			}
			star5+="\n";
		}
		return star5;
	}
	
}

