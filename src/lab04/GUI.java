package lab04;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class GUI extends JFrame {
	JFrame frame = new JFrame();	
	JButton button = new JButton("Run");
	JTextArea textIn = new JTextArea();
	JTextArea textOut = new JTextArea();
	Control con = new Control();
	Allstar all = new Allstar();
	String choice[] = {"star1", "star2", "star3", "star4", "star5" };
	JComboBox box = new JComboBox(choice);

	public GUI() {
		setSize(260,300);		
		setLayout(null);
		
		button.setBounds(150, 10, 80, 20);
		textIn.setBounds(80, 10,50, 20);
		textOut.setBounds(20, 50, 200, 200);
		box.setBounds(10, 10, 60, 20);
				
		add(box);
		add(textOut);
		add(textIn);
		add(button);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (box.getSelectedItem() == "star1") {
					textOut.setText(con.build1(Integer.parseInt(textIn
							.getText())));
				}
				if (box.getSelectedItem() == "star2") {
					textOut.setText(con.build2(Integer.parseInt(textIn
							.getText())));
				}
				if (box.getSelectedItem() == "star3") {
					textOut.setText(con.build3(Integer.parseInt(textIn
							.getText())));
				}
				if (box.getSelectedItem() == "star4") {
					textOut.setText(con.build4(Integer.parseInt(textIn
							.getText())));
				}
				if (box.getSelectedItem() == "star5") {
					textOut.setText(con.build5(Integer.parseInt(textIn
							.getText())));

				}
			}

		});

	}

}
